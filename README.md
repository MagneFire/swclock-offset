# swclock-offset
Some devices have a working but non-writable real-time clock (RTC).
This repository contains two shell scripts: One writes the offset between
'hwclock' and 'swclock' to a file at shutdown, another one reads the
offset from the file at boot and sets the 'swclock'. This way the system
time in userspace is kept in present time.

## Build
The repository contains a simple Makefile. By `make install` the two
shell scripts get installed to `/sbin`. Adding `PREFIX=/usr` places them
to `/usr/sbin`. Variable `DESTDIR` allows to change the install path to
something like `DESTDIR=$pkgdir`.

To install the two OpenRC service files, perform `make install_openrc`.
They get installed to `/etc/init.d`. Variable `DESTDIR` applies here as
well, `PREFIX` does not.

## Managing OpenRC services
After installing the files, OpenRC services need to be set accordingly.

### Replace service hwclock by osclock
The service "osclock" is a dummy service simply providing "clock".
This avoids other services that need "clock" to call the service
"hwclock".
```
rc-update -q del hwclock boot
rc-update -q add osclock boot
```

### Assign swclock-offset services to runlevels "boot" and "shutdown"
The service "swclock-offset-boot" needs to run after the sysfs has
been mounted. As the sysfs is mounted in runlevel sysinit, assigning
the service to runlevel boot is enough to keep the order.
```
rc-update -q add swclock-offset-boot boot
rc-update -q add swclock-offset-shutdown shutdown
```

### Update dependency tree cache
Before installation of the package "swclock-offset", the system time
might jump back and forth. Because of this, OpenRC can get confused
whether the cached dependency tree is old or new. To avoid this
uncertainty, trigger an update of the dependency tree cache.
```
rc-update -q --update
```

### On package removal
Restore the original state of the services.
```
rc-update -q del swclock-offset-boot boot
rc-update -q del swclock-offset-shutdown shutdown

rc-update -q del osclock boot
rc-update -q add hwclock boot
```

## systemd service files
Files for systemd are not available yet. Basically they should be set
up in a similar way like the OpenRC service files.

The script `swclock-offset-boot` needs to be executed once during boot,
after sysfs becomes available (because it reads `/sys/class/rtc/rtc0/since_epoch`)
and before fsck (because fsck complains if timestamps are inconsistant).

On shutdown, the script `swclock-offset-shutdown` needs to be executed
once. This needs to be done before the filesystems get unmounted because
the script writes the offset to the file `/var/cache/swclock-offset/offset-storage`.

Additionally, like on OpenRC, the usual synchronization between hwclock
and swclock needs to be disabled (likely some other systemd service that
needs to be disabled).

## Delete cache file and folder on package removal
Independent of the init system, on package removal the cache file and
its folder should be removed as a clean-up action. This can be done by
e.g.:

```
#!/bin/sh

offset_file="/var/cache/swclock-offset/offset-storage"
offset_directory="/var/cache/swclock-offset"

if [ -f $offset_file ]; then
	rm $offset_file
fi

if [ -d $offset_directory ]; then
	rmdir $offset_directory
fi
```
