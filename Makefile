install:
	install -Dm 755 src/swclock-offset-boot.sh \
		$(DESTDIR)$(PREFIX)/sbin/swclock-offset-boot
	install -Dm 755 src/swclock-offset-shutdown.sh \
		$(DESTDIR)$(PREFIX)/sbin/swclock-offset-shutdown

install_openrc:
	install -Dm 755 openrc/swclock-offset-boot.initd \
		$(DESTDIR)/etc/init.d/swclock-offset-boot
	install -Dm 755 openrc/swclock-offset-shutdown.initd \
		$(DESTDIR)/etc/init.d/swclock-offset-shutdown
